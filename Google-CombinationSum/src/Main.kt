/*
* -------
* Google
* -------
* Given a list of integers S and a target number k, write a function that returns a subset of S that adds up to k.
* If such a subset cannot be made, then return null.
* Integers can appear more than once in the list. You may assume all numbers in the list are positive.
* For example, given S = [12, 1, 61, 5, 9, 2] and k = 24, return [12, 9, 2, 1] since it sums up to 24.
*
* */

class Solution() {
    var target = -1
    var inputList: MutableList<Int> = mutableListOf()
    var found = false
    var foundList: MutableList<Int>? = null

    fun solve(_inputList: List<Int>, _target: Int) {
        inputList = _inputList.toMutableList().apply { sort() }
        target = _target
        backtrack(mutableListOf(), 0)
    }

    fun backtrack(indexHolder: MutableList<Int>, sum: Int) {
//    print("indexHolder: $indexHolder | sum: $sum | (")
//    if (indexHolder.isNotEmpty()) {
//        indexHolder.forEach {
//            print("${inputList2[it]}, ")
//        }
//    }
//
//    println(")")
        if (found) return

        if (sum == target) {
            found = true
            foundList = mutableListOf()
            indexHolder.forEach {
                foundList?.add(inputList[it])
            }
        }
        val lastIndex = if (indexHolder.isEmpty()) {
            -1
        } else {
            indexHolder.last()
        }
        for (i in lastIndex until inputList.size - 1) {
            backtrack(indexHolder.toMutableList().apply { add(i + 1) }, sum + inputList[i + 1])
        }
    }

}

class Input(val list: List<Int>, val target: Int)
class TestCase(val input: Input, val expected: List<Int>?)

fun main() {

    val testList = listOf(
        TestCase(Input(listOf(1, 3, 5, 7, 9, 11), 21), listOf(1, 9, 11)),
        TestCase(Input(listOf(11, 3, 5, 7, 9, 1), 21), listOf(1, 9, 11)),
        TestCase(Input(listOf(3, 3, 3, 3, 5, 7, 9, 1), 21), listOf(1, 3, 3, 5, 9)),
        TestCase(Input(listOf(323, 123213, 1231233, 1231233, 43425, 237, 2329, 1111), 999), null),
        TestCase(Input(listOf(323, 123213, 1231233, 1231233, 43425, 237, 2329, 1111), 1434), listOf(323, 1111)),
        TestCase(Input(listOf(12, 1, 61, 5, 9, 2), 24), listOf(323, 1111)),
        TestCase(Input(listOf(3, 62, 33, 33, 12, 4, 1, 1, 1, 1, 7, 8, 2, 10), 66), listOf(33, 33))
    )
    testList.forEachIndexed { index, value ->
        value.apply {
            println("test #${index}:  input = ${input.list}, target = ${input.target}")
            println("expected: ${expected}")
            val solution = Solution().apply { solve(input.list, input.target) }
            println("actual  : ${solution.foundList}")
            println()
        }
    }
//    val list = listOf<Int>()
//    solution.sum(list, 9)
}